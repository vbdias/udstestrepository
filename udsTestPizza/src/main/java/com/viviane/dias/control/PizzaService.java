package com.viviane.dias.control;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import com.viviane.dias.model.ItemPersonalizacao;
import com.viviane.dias.model.Pizza;
import com.viviane.dias.model.Sabor;
import com.viviane.dias.model.Tamanho;
import com.viviane.dias.model.dao.ItemPersonalizacaoDao;
import com.viviane.dias.model.dao.PizzaDao;
import com.viviane.dias.model.dao.SaborDao;
import com.viviane.dias.model.dao.TamanhoDao;

@Service
public class PizzaService {

	@Autowired
	private PizzaDao pizzaDao;
	
	@Autowired
	private SaborDao saborDao;
	
	@Autowired
	private TamanhoDao tamanhoDao;
	
	@Autowired
	private ItemPersonalizacaoDao itemPersonalizacaoDao;
	
	public Pizza montaPizza(String tamanho, String sabor) {
		try {
			Tamanho t = tamanhoDao.getTamanhoByDescricao(tamanho);
			Sabor s = saborDao.getSaborByDescricao(sabor);
			Pizza pizza = new Pizza();
			pizza.setTamanho(t);
			pizza.setSabor(s);
			return pizzaDao.save(pizza);
		} catch (EmptyResultDataAccessException e) {
			throw new IllegalArgumentException("Parametros de entrada nao sao validos para montar a pizza");
		} 
	}
	
	public Pizza adicionaItensPersonalizacaoPizza(List<String> itens, Pizza pizza) {
		try {
			for (String strItem : itens) {
				ItemPersonalizacao item = itemPersonalizacaoDao.getItemByDescricao(strItem);
				pizza.addItemPersonalizacao(item);
			}
			return pizzaDao.update(pizza);
		} catch (EmptyResultDataAccessException e) {
			throw new IllegalArgumentException("Parametros de entrada nao sao validos como itens para personalizacao da pizza");
		}
	}
	
	public Pizza getPizza(Long pizzaId) {
		try {
			return pizzaDao.getByID(pizzaId);
		} catch (EmptyResultDataAccessException e) {
			throw new IllegalArgumentException("Nao foi encontrado nenhuma pizza com esse identificador: " + pizzaId);
		}
	}

}

