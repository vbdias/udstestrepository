package com.viviane.dias.control;

import java.io.IOException;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestHeader;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.google.gson.Gson;
import com.viviane.dias.model.Pizza;
import com.viviane.dias.model.dto.ResumoDto;

@RestController
@RequestMapping("/service")
public class UdsWs {


	
	@Autowired
	private PizzaService pizzaService;
	
	
	@RequestMapping(value = "/buildPizza", method = RequestMethod.POST, consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public String buildPizza(@RequestParam String tamanho, @RequestParam String sabor) {
		Pizza pizza = pizzaService.montaPizza(tamanho, sabor);
		return new Gson().toJson(pizza);
	}
	
	@RequestMapping(value = "/configItems", method = RequestMethod.POST, consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public String personalizarPizza(@RequestParam List<String> itens, @RequestHeader Long pizzaId) {
		Pizza pizza = pizzaService.getPizza(pizzaId);
		if(pizza != null && itens != null) {
			pizza = pizzaService.adicionaItensPersonalizacaoPizza(itens, pizza);
		}
		return new Gson().toJson(pizza);
	}
	
	@RequestMapping(value = "/resumo/{pizza_id}", method = RequestMethod.GET, consumes=MediaType.APPLICATION_JSON_VALUE, produces= MediaType.APPLICATION_JSON_VALUE)
	public String get(@PathVariable(value = "pizza_id") Long pizzaId) {
		Pizza pizza = pizzaService.getPizza(pizzaId);
		ResumoDto dto = new ResumoDto(pizza);
		return new Gson().toJson(dto);
	}

	@ExceptionHandler
	void handleIllegalArgumentException(IllegalArgumentException e, HttpServletResponse response) throws IOException {
		response.sendError(HttpStatus.BAD_REQUEST.value());
	}
}
