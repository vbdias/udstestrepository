package com.viviane.dias;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UdsTestPizzaApplication {

	public static void main(String[] args) {
		SpringApplication.run(UdsTestPizzaApplication.class, args);
	}

}
