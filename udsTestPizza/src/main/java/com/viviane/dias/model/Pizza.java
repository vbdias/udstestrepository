

package com.viviane.dias.model;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.*;

@Entity
@Table(name="PIZZAS")
public class Pizza {

	@Id
	@Column(name="ID")
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="PIZZA_SEQ")
	@SequenceGenerator(name="PIZZA_SEQ", sequenceName="PIZZA_SEQ", initialValue=1, allocationSize=1)
	private Long id;
	
	@ManyToOne
	@JoinColumn(name="ID_TAMANHO", referencedColumnName="ID")
	private Tamanho tamanho;
	
	@ManyToOne
	@JoinColumn(name="ID_SABOR", referencedColumnName="ID")
	private Sabor sabor;
	
	@OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	@JoinTable(name="PIZZA_ITENS_PERSONALIZACAO",joinColumns={@JoinColumn(name = "ID_PIZZA")},inverseJoinColumns={@JoinColumn(name = "ID_ITEM_PERSONALIZACAO")})
	private List<ItemPersonalizacao> personalizacoes;
	
	public Tamanho getTamanho() {
		return tamanho;
	}

	public void setTamanho(Tamanho tamanho) {
		this.tamanho = tamanho;
	}

	public Sabor getSabor() {
		return sabor;
	}

	public void setSabor(Sabor sabor) {
		this.sabor = sabor;
	}

	public List<ItemPersonalizacao> getPersonalizacoes() {
		return personalizacoes;
	}

	public void setPersonalizacoes(List<ItemPersonalizacao> personalizacoes) {
		this.personalizacoes = personalizacoes;
	}

	public void addItemPersonalizacao(ItemPersonalizacao item) {
		if(personalizacoes == null) {
			personalizacoes = new ArrayList<ItemPersonalizacao>();
		}
		personalizacoes.add(item);
	}


}
