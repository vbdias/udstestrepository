package com.viviane.dias.model.dao;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.Query;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;

@Transactional
public abstract class AbstractJpaDao<Entidade> {

	private final Class<Entidade> entityClass;
	
	@Autowired
	private EntityManager manager;
	
	public AbstractJpaDao(final Class<Entidade> entityClass) {
		this.entityClass = entityClass;
	}
	
	
	protected abstract String queryFindAll();
	
	
	public List<Entidade> listAll(){
		return manager.createQuery(queryFindAll()).getResultList();
	}
	
	@Transactional
	private Entidade getOnlyOne (Query query) {
		List<Entidade> list = query.getResultList();
		query.setMaxResults(1);
		if(list != null && !list.isEmpty() && list.size() ==1) {
			return list.get(0);
		}else {
			return null;
		}
	}
	
	@Transactional
	public Entidade load(Object id) {
		return manager.find(entityClass, id);
	}
	
	@Transactional
	public Entidade save(Entidade entity) {
		if(manager.contains(entity)) {
			entity = update(entity);
		}else {
			manager.persist(entity);
			manager.flush();
		}
		return entity;
	}


	public Entidade update(Entidade entity) {
		return manager.merge(entity);
	}
	
	public Query createQuery (String query) {
		return manager.createQuery(query);
	}
	
	
}
