package com.viviane.dias.model.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.viviane.dias.model.Tamanho;

@Repository
public class TamanhoDao extends AbstractJpaDao<Tamanho>{

	public TamanhoDao() {
		super(Tamanho.class);
	}

	@Override
	protected String queryFindAll() {
		return "select o from Tamanho ";
	}

	public Tamanho getTamanhoByDescricao(String descricao) {
		Query query = createQuery(" select t from Tamanho t where t.descricao = :descricao");
		query.setParameter("descricao", descricao);
		return (Tamanho) query.getSingleResult();
	}
	
	
	
	
}
