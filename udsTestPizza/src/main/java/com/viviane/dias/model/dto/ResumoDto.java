package com.viviane.dias.model.dto;

import java.util.ArrayList;
import java.util.List;

import com.viviane.dias.model.ItemPersonalizacao;
import com.viviane.dias.model.Pizza;

public class ResumoDto {

	private String tamanho;
	
	private String sabor;
	
	private Double custoTotal;
	
	private Integer tempoPreparo;
	
	private List<ItemDto> personalizacao;

	
	public ResumoDto(Pizza pizza) {
		tamanho = pizza.getTamanho().getDescricao();
		sabor = pizza.getSabor().getDescricao();
		if(pizza.getPersonalizacoes() != null) {
			personalizacao = new ArrayList<ItemDto>();
			pizza.getPersonalizacoes().forEach(item -> {
				personalizacao.add(new ItemDto(item));
			});
		}
		calculaTempoPreparo(pizza);
		calculaCustoTotal(pizza);
		
	}
	private void calculaCustoTotal(Pizza pizza) {
		Double custoTotal = new Double(0);
		custoTotal += pizza.getTamanho().getValor();
		if(pizza.getPersonalizacoes() != null) {
			for (ItemPersonalizacao item : pizza.getPersonalizacoes()) {
				custoTotal += item.getCustoAdicional();	
			}
		}
		this.custoTotal = custoTotal;
	}

	private void calculaTempoPreparo(Pizza pizza) {
		Integer tempoPreparo = new Integer(0);
		tempoPreparo += pizza.getTamanho().getTempoPreparo();
		tempoPreparo += pizza.getSabor().getTempoAdicional();
		if(pizza.getPersonalizacoes() != null) {
			for (ItemPersonalizacao item : pizza.getPersonalizacoes()) {
				tempoPreparo += item.getTempoAdicional();
			}
		}
		this.tempoPreparo = tempoPreparo;
	}

	
	public String getTamanho() {
		return tamanho;
	}

	public String getSabor() {
		return sabor;
	}

	public Double getCustoTotal() {
		return custoTotal;
	}

	public Integer getTempoPreparo() {
		return tempoPreparo;
	}

	public List<ItemDto> getPersonalizacao() {
		return personalizacao;
	}



	
	
	
	
	
}
