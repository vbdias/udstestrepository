package com.viviane.dias.model.dto;

import com.viviane.dias.model.ItemPersonalizacao;

public class ItemDto {
	
	private String item;
	private Double custoAdicional;
	
	public ItemDto(ItemPersonalizacao item) {
		this.item = item.getDescricao();
		this.custoAdicional = item.getCustoAdicional();
	}
	public String getItem() {
		return item;
	}
	public void setItem(String item) {
		this.item = item;
	}
	public Double getCustoAdicional() {
		return custoAdicional;
	}
	public void setCustoAdicional(Double custoAdicional) {
		this.custoAdicional = custoAdicional;
	}
	
	
	

}
