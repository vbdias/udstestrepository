package com.viviane.dias.model.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.viviane.dias.model.ItemPersonalizacao;

@Repository
public class ItemPersonalizacaoDao extends AbstractJpaDao<ItemPersonalizacao>{

	public ItemPersonalizacaoDao() {
		super(ItemPersonalizacao.class);
	}

	@Override
	protected String queryFindAll() {
		return "select i from ItemPersonalizacao ";
	}

	public ItemPersonalizacao getItemByDescricao(String descricao) {
		Query query = createQuery(" select p from ItemPersonalizacao p where p.descricao = :descricao");
		query.setParameter("descricao", descricao);
		return (ItemPersonalizacao) query.getSingleResult();
	}
	
	
	
	
}
