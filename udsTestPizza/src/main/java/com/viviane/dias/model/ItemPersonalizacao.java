package com.viviane.dias.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name="ITENS_PERSONALIZACAO")
public class ItemPersonalizacao {

	
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name="DESCRICAO")
	private String descricao;
	
	@Column(name="CUSTO_ADICIONAL")
	private Double custoAdicional;
	
	@Column(name="TEMPO_ADICIONAL")
	private Integer tempoAdicional;
	
	
	public String getDescricao() {
		return descricao;
	}


	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}


	public void setCustoAdicional(Double custoAdicional) {
		this.custoAdicional = custoAdicional;
	}


	public void setTempoAdicional(Integer tempoAdicional) {
		this.tempoAdicional = tempoAdicional;
	}

	public double getCustoAdicional() {
		return custoAdicional;
	}


	public void setCustoAdicional(double custoAdicional) {
		this.custoAdicional = custoAdicional;
	}


	public int getTempoAdicional() {
		return tempoAdicional;
	}


	public void setTempoAdicional(int tempoAdicional) {
		this.tempoAdicional = tempoAdicional;
	}
	
	
}
