package com.viviane.dias.model.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.viviane.dias.model.Pizza;

@Repository
public class PizzaDao extends AbstractJpaDao<Pizza>{

	public PizzaDao() {
		super(Pizza.class);
	}

	@Override
	protected String queryFindAll() {
		return "select o from Pizza ";
	}

	public Pizza getByID(Long id) {
		Query query = createQuery(" select p from Pizza p where p.id = :id");
		query.setParameter("id", id);
		return (Pizza) query.getSingleResult();
	}
	
	
	
	
}
