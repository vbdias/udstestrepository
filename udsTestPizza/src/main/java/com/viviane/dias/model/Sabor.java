package com.viviane.dias.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import com.google.gson.annotations.Expose;

@Entity
@Table(name="SABORES")
public class Sabor{
	
	@Id
	@Column(name="ID")
	private Long id;
	
	@Column(name = "DESCRICAO")
	private String descricao;
	
	@Column(name = "TEMPO_ADICIONAL")
	private int tempoAdicional;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getDescricao() {
		return descricao;
	}

	public void setDescricao(String descricao) {
		this.descricao = descricao;
	}

	public int getTempoAdicional() {
		return tempoAdicional;
	}

	public void setTempoAdicional(int tempoAdicional) {
		this.tempoAdicional = tempoAdicional;
	}
	
	@Override
	public String toString() {
		return "Sabor:" + descricao;
	}
	
	
}
