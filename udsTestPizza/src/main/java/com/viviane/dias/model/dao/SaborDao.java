package com.viviane.dias.model.dao;

import javax.persistence.Query;

import org.springframework.stereotype.Repository;

import com.viviane.dias.model.Sabor;


@Repository
public class SaborDao extends AbstractJpaDao<Sabor>{

	public SaborDao() {
		super(Sabor.class);
	}

	@Override
	protected String queryFindAll() {
		return "select o from Sabor ";
	}

	public Sabor getSaborByDescricao(String descricao) {
		Query query = createQuery(" select s from Sabor s where s.descricao = :descricao");
		query.setParameter("descricao", descricao);
		return (Sabor) query.getSingleResult();
	}
	
	
	
	
}
